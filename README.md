Atividade 5 do laboratorio 1 da disciplina computacao concorrente.

Cria um vetor com N inteiros e calcula o quadrado de todos eles, usando computacao concorrente(M threads).



Na minha implementacao, pode-se setar o tamanho do vetor modificando a constante VECTOR_SIZE,
e o numero de thread com a constante NTHREADS.


O programa ira distribuir a tarefa de elevar o vetor ao quadrado de acordo, divindo setores do vetor entre as threads.

No final, o programa calcula tudo para um vetor de teste, e checa se esta tudo certo.


PS: O programa falha para vetores com tamanho igual a 1 bilhao, por motivos desconhecidos ate o momento.